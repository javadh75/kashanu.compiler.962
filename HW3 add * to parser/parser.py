
tokens = ('NUMBER', 'PLUS', 'MULTI',)


t_PLUS    = r'\+'
t_MULTI   = r'\*'


def t_NUMBER(t):
  r'\d+'
  t.value = int(t.value)
  return t


t_ignore = " \t"

def t_error(t):
  print("Illegal character '%s'" % t.value[0])
  t.lexer.skip(1)


import ply.lex as lex
lex.lex()


def p_e_e_e_a(p):
  """E : E PLUS MULTIPLICATION
  | MULTIPLICATION"""
  if len(p) == 4:
    p[0] = p[1]+p[3]
  else:
    p[0] = p[1]
  print('کاهش با قانون E → E + M | M ',p[0])

def p_e_e_a(p):
  """MULTIPLICATION : MULTIPLICATION MULTI NUM
  | NUM"""
  if len(p) == 4:
    p[0] = p[1]*p[3]
  else:
    p[0] = p[1]
  print('کاهش با قانون M → M * NUM | NUM  ',p[0])
def p_e_a(p):
  'NUM : NUMBER'
  p[0]=p[1]
  print('کاهش با قانون NUM → a  ',p[1])
def p_error(p):
  print("Syntax error at '%s'" % p.value)

import ply.yacc as yacc; yacc.yacc()

while True:
  s = input('calc > ')
  if s.strip()=='':break
  yacc.parse(s)
