#include <iostream>

int main (void)
{
    char S[1000];
    std::cout << "Enter String: ";
    std::cin >> S;
    char O[1000];
    int sptr = 0;
    int optr = 0;
    while (S[sptr] != 0)
    {
        if (S[sptr] == '*' || S[sptr] == '/' || S[sptr] == '-' || S[sptr] == '+' || S[sptr] == '(' || S[sptr] == ')')
        {
            O[optr] = S[sptr];
            optr++;
        }
        if (S[sptr] >= '0' || S[sptr] <= '9')
        {
            if (O[optr-1] != 'a')
            {
                O[optr] = 'a';
                optr++;
            }
        }
        sptr++;
    }
    O[optr] = 0;
    std::cout << "Resulat: " << O << std::endl;
    return 0;
}