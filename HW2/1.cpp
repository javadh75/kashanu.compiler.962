#include <iostream>

int TOKEN_SIZE = 10;

enum tType {
                NOTHING,
                PLUS,
                MINUS,
                MULTIPLY,
                DIVID,
                LPARENTHESES,
                RPARENTHESES,
                NUMBER
            };

struct Token
{
    tType t = NOTHING;   // type
    double v = 0;  // value
};

void printToken(Token *);

int main (void)
{
    char S[1000];
    std::cout << "Enter String: ";
    std::cin >> S;
    Token O[TOKEN_SIZE];
    int sptr = 0;
    int optr = 0;
    int temp = 0;
    while (S[sptr] != 0)
    {
        if (S[sptr] == '*' || S[sptr] == '/' || S[sptr] == '-' || S[sptr] == '+' || S[sptr] == '(' || S[sptr] == ')')
        {
            switch(S[sptr])   
            {
                case '+': O[optr].t = PLUS; break;
                case '-': O[optr].t = MINUS; break;
                case '*': O[optr].t = MULTIPLY; break;
                case '/': O[optr].t = DIVID; break;
                case '(': O[optr].t = LPARENTHESES; break;
                case ')': O[optr].t = RPARENTHESES; break;
            }
            optr++;
        }
        if (S[sptr] >= '0' && S[sptr] <= '9')
        {
            if (S[sptr+1] >= '0' && S[sptr+1] <= '9')
            {
                temp = temp * 10 + (S[sptr] - '0');
            }
            else
            {
                temp = temp * 10 + (S[sptr] - '0');
                O[optr].t = NUMBER;
                O[optr].v = temp;
                optr++;
                temp = 0;
            }
        }
        sptr++;
    }
    printToken(O);
    return 0;
}

void printToken(Token * T)
{
    for (int i = 0; i < TOKEN_SIZE; i++)
    {
        switch(T[i].t)
        {
            case NOTHING: std::cout << "Type: Nothing" << std::endl; break;
            case PLUS: std::cout << "Type: PLUS" << std::endl; break;
            case MINUS: std::cout << "Type: MINUS" << std::endl; break;
            case MULTIPLY: std::cout << "Type: MULTIPLY" << std::endl; break;
            case DIVID: std::cout << "Type: DIVID" << std::endl; break;
            case LPARENTHESES: std::cout << "Type: LPARENTHESES" << std::endl; break;
            case RPARENTHESES: std::cout << "Type: RPARENTHESES" << std::endl; break;
            case NUMBER: std::cout << "Type: NUMBER   Value: " << T[i].v << std::endl; break;
        }

    }
}